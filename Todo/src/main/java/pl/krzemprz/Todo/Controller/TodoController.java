package pl.krzemprz.Todo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.krzemprz.Todo.Entity.Note;
import pl.krzemprz.Todo.Servis.TodoService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class TodoController {

    @Autowired
    private TodoService service;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    private Flux<Note> getAllNotes() {
        return service.findAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    private void saveNote(@RequestBody Note note) {
        Mono<Note> monoNote = service.save(note);

        monoNote.map(id -> id.getId())
                .subscribe(System.out::println);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PATCH)
    private void updateNote(@RequestBody Note note, @PathVariable long id){
        Mono<Note> monoNote = service.update(note, id);

        monoNote.map(x -> x.getContent())
                .subscribe(System.out::println);
    }

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    private String test() {
        return "TEST";
    }
}
