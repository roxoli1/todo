package pl.krzemprz.Todo.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.krzemprz.Todo.Entity.Note;
import reactor.core.publisher.Mono;

@Repository
public interface TodoRepository extends CrudRepository<Note, Long> {
    Note save(Note note);
}
