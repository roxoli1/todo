package pl.krzemprz.Todo.Entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "Notka")
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    long id;
    @Length(max = 30)
    @NotNull
    @Column(name = "subject")
    String subject;
    @Column(name = "content")
    String content;
    @Column(name = "creation_date")
    LocalDateTime creationDate;
    @Column(name = "modification_date")
    LocalDateTime modyficationDate;

    // Getters and setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getModyficationDate() {
        return modyficationDate;
    }

}
