package pl.krzemprz.Todo.Servis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krzemprz.Todo.Entity.Note;
import pl.krzemprz.Todo.Repository.TodoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository repository;

    @Override
    public Flux<Note> findAll() {
        return Flux.fromIterable(repository.findAll());
    }

    @Override
    public Mono<Note> save(Note note) {
        return Mono.just(repository.save(note));
    }

    @Override
    public Mono<Note> update(Note note, long id) {
        Optional<Note> optNote = repository.findById(id);
        optNote.get().setContent(note.getContent());
        optNote.get().setSubject(note.getSubject());
        return Mono.just(repository.save(optNote.get()));
    }
}
