package pl.krzemprz.Todo.Servis;

import pl.krzemprz.Todo.Entity.Note;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TodoService {
    Flux<Note> findAll();
    Mono<Note> save(Note note);
    Mono<Note> update(Note note, long id);
}
